# dt-sns 

[JEESNS](https://gitee.com/zchuanzhao/jeesns)基础上做的修改，功能介绍参见jeesns即可。

## 主要改动如下：

1. 后端框架更换为Spring Boot。
2. 不再使用maven多模块，简化了开发和部署。
3. 日志打印改为slf4j框架，使用logback作为实现。

## 部署说明

1. 创建数据库。如使用MySQL，字符集选择为`utf8`或者`utf8mb4`（支持更多特殊字符，推荐）。
2. 执行数据库脚本。数据库脚本在`/src/main/resources/database`目录下。
3. 在eclipse中导入maven项目。点击eclipse菜单`File` - `Import`，选择`Maven` - `Existing Maven Projects`。
4. 设置项目编码为utf-8，选择jdk1.7版本或以上，不要选择jre。
5. 修改数据库连接，在`/src/main/resources/application.propertis`中修改。
6. 点击运行 com.dt.sns.WebAppStart 即可启动
7. 访问系统。访问端口和路径在application.propertis设置：[http://localhost:{server.port}/{server.contextPath}/];
8. 在application.propertis中设置上传文件路径：fileUploadPath={E:/EProject/datatruth/dtsns/}
9. 后台管理访问路径：[http://localhost:{server.port}/{server.contextPath}/manage]，登陆账户和密码：admin/123456

## 部署详细说明

1. [JDK安装说明](https://my.oschina.net/zchuanzhao/blog/853387)
2. [Maven安装说明](https://my.oschina.net/zchuanzhao/blog/853392)
3. [dt-sns使用Maven打包介绍]
4. [dt-sns导入数据库说明]
5. [Eclipse导入dt-sns项目和部署说明]

## 讨论群
QQ群：659359876