package com.dt.lucene;

import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class IndexText {

	public static void main(String[] args) {
		add();

	}
	public static void del(int docid) {
		
	}
	public static void add() {
		try {
			String indexDir = "D:\\data\\Lucene\\textIndex";

			// 1、打开存储目录，这里使用的是文本系统存储的，对应内存存储：RAMDirectory
			Directory dir = FSDirectory.open(Paths.get(indexDir));

			// 2、使用标准通用的分析器，如果要对处理中文，请使用IK、庖丁，否则分词不准确
			Analyzer analyzer = new StandardAnalyzer();

			// 3、索引配置信息
			IndexWriterConfig conf = new IndexWriterConfig(analyzer);

			// 4、构建索引器
			IndexWriter indexWriter = new IndexWriter(dir, conf);
			
//			indexWriter.deleteAll();
//			indexWriter.commit();

			for (int i = 0; i < 1000; i++) {

				String text = "Welcome to Indexed world. It's very interesting:" + i;

				// 5、构造待索引的文档
				Document doc = new Document();
				doc.add(new Field("content", text, TextField.TYPE_STORED));

				// 6、将文本添加到索引中
				indexWriter.addDocument(doc);

				if (i % 100 == 0)
					indexWriter.commit();

			}
			// 7、释放资源
			indexWriter.close();
			dir.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("end");
	}

}