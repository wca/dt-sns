<header class="main-header">
    <a href="http://www.jeesns.cn" class="logo" target="_blank">
        <span class="logo-mini"><b>JEE</b></span>
        <span class="logo-lg"><b>JEESNS</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="${basePath}${loginUser.avatar}" class="user-image" alt="User Image">
                        <span class="hidden-xs">${loginUser.name}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="${basePath}${loginUser.avatar}" class="img-circle" alt="User Image">
                            <p>
                                ${loginUser.name}
                                <small>${loginUser.email}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="${basePath}/member/editInfo" class="btn btn-default btn-flat" target="_blank">设置</a>
                            </div>
                            <div class="pull-right">
                                <a href="${basePath}/member/logout" class="btn btn-default btn-flat">退出</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>