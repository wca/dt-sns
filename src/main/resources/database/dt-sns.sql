/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.17 : Database - dtsns
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dt-sns` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dt-sns`;

/*Table structure for table `tbl_action` */

DROP TABLE IF EXISTS `tbl_action`;

CREATE TABLE `tbl_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `log` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '状态，0正常，1禁用',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10004 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_action` */

insert  into `tbl_action`(`id`,`create_time`,`name`,`log`,`status`,`update_time`) values (1,'2017-11-03 17:23:41','会员注册','注册了账号',0,'2017-11-27 18:36:22'),(2,'2017-11-03 17:23:41','会员登录','登录了账号',0,'2017-11-27 18:36:26'),(3,'2017-11-03 17:23:41','修改密码','修改了密码',0,'2017-11-03 17:23:41'),(4,'2017-11-03 17:23:41','找回密码','找回了密码',0,'2017-11-03 17:23:41'),(5,'2017-11-03 17:23:41','登录失败','登录失败',0,'2017-11-03 17:23:41'),(3001,'2017-11-03 17:23:41','删除微博','删除了微博',0,'2017-11-03 17:23:41'),(3002,'2017-11-03 17:23:41','删除微博评论','删除了微博评论',0,'2017-11-03 17:23:41'),(3003,'2017-11-03 17:23:41','删除群组','删除了群组',0,'2017-11-03 17:23:41'),(3004,'2017-11-03 17:23:41','删除群组帖子','删除了帖子',0,'2017-11-03 17:23:41'),(3005,'2017-11-03 17:23:41','删除群组帖子评论','删除了帖子评论',0,'2017-11-03 17:23:41'),(3006,'2017-11-03 17:23:41','删除文章','删除文章',0,'2017-11-03 17:23:41'),(3007,'2017-11-03 17:23:41','删除文章评论','删除了文章评论',0,'2017-11-03 17:23:41'),(10001,'2017-11-03 17:23:41','发布微博','发布了微博',0,'2017-11-03 17:23:41'),(10002,'2017-11-03 17:23:41','群组发帖','发布了群组帖子',0,'2017-11-03 17:23:41'),(10003,'2017-11-03 17:23:41','发布文章','发布了文章',0,'2017-11-03 17:23:41');

/*Table structure for table `tbl_action_log` */

DROP TABLE IF EXISTS `tbl_action_log`;

CREATE TABLE `tbl_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '0',
  `foreign_id` int(11) DEFAULT '0',
  `action_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_action_log_member` (`member_id`),
  KEY `fk_action_log_action` (`action_id`),
  CONSTRAINT `fk_action_log_action` FOREIGN KEY (`action_id`) REFERENCES `tbl_action` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_action_log_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_action_log` */

insert  into `tbl_action_log`(`id`,`create_time`,`member_id`,`action_id`,`remark`,`type`,`foreign_id`,`action_ip`) values (1,'2017-11-22 14:37:19',NULL,5,'??????admin??????admin',0,0,'0:0:0:0:0:0:0:1'),(2,'2017-11-27 18:47:56',1,2,'',0,0,'0:0:0:0:0:0:0:1'),(3,'2017-11-27 19:07:14',1,10003,'',1,1,'0:0:0:0:0:0:0:1'),(4,'2018-01-08 17:37:23',1,2,'',0,0,'0:0:0:0:0:0:0:1'),(5,'2018-01-09 09:46:13',1,10003,'',1,2,'0:0:0:0:0:0:0:1'),(6,'2018-03-12 09:15:49',NULL,5,'登录用户名：dtsns，登录密码：dtsns',0,0,'0:0:0:0:0:0:0:1'),(7,'2018-03-12 09:16:06',NULL,5,'登录用户名：dtsns，登录密码：dtsns',0,0,'0:0:0:0:0:0:0:1'),(8,'2018-03-12 09:16:12',NULL,5,'登录用户名：dtsns，登录密码：123456',0,0,'0:0:0:0:0:0:0:1'),(9,'2018-03-12 09:16:20',NULL,5,'登录用户名：admin，登录密码：123456',0,0,'0:0:0:0:0:0:0:1'),(10,'2018-03-12 09:17:11',NULL,5,'登录用户名：admin，登录密码：admin',0,0,'0:0:0:0:0:0:0:1'),(11,'2018-03-12 09:34:29',NULL,5,'登录用户名：admin，登录密码：123456',0,0,'0:0:0:0:0:0:0:1'),(12,'2018-03-12 09:35:18',1,3,'',0,0,'0:0:0:0:0:0:0:1'),(13,'2018-03-12 09:35:29',1,2,'',0,0,'0:0:0:0:0:0:0:1');

/*Table structure for table `tbl_ads` */

DROP TABLE IF EXISTS `tbl_ads`;

CREATE TABLE `tbl_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1是图片链接，2是文字链接，3是代码',
  `name` varchar(100) DEFAULT NULL COMMENT '广告名称',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `content` varchar(1000) NOT NULL COMMENT '内容，如果是图片链接，该内容为图片地址，如果是文字链接，改内容是文字描述信息，如果是代码，改内容是广告代码',
  `link` varchar(255) DEFAULT NULL COMMENT '链接，图片链接和文字链接类型时才有效',
  `status` int(1) DEFAULT '0' COMMENT '状态，0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_ads` */

/*Table structure for table `tbl_archive` */

DROP TABLE IF EXISTS `tbl_archive`;

CREATE TABLE `tbl_archive` (
  `archive_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_type` int(11) DEFAULT '0' COMMENT '发布类型，1是普通文章，2是群组文章',
  `title` varchar(255) DEFAULT NULL COMMENT '文档标题',
  `member_id` int(11) DEFAULT NULL COMMENT '会员ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `description` varchar(255) DEFAULT NULL COMMENT '描述说明',
  `keywords` varchar(100) DEFAULT NULL COMMENT '关键词',
  `view_rank` int(11) DEFAULT '0' COMMENT '浏览权限，0不限制，1会员',
  `view_count` int(11) DEFAULT '0' COMMENT '浏览次数',
  `writer` varchar(30) DEFAULT '' COMMENT '作者',
  `source` varchar(30) DEFAULT '' COMMENT '来源',
  `pub_time` datetime DEFAULT NULL COMMENT '发布日期',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `thumbnail` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `last_reply` datetime DEFAULT NULL COMMENT '最后回复时间',
  `can_reply` int(1) DEFAULT '0' COMMENT '是否可以回复，0可以回复，1不可以回复',
  `good_num` int(11) DEFAULT '0' COMMENT '点赞数量',
  `bad_num` int(11) DEFAULT '0' COMMENT '踩数量',
  `check_admin` int(11) DEFAULT '0' COMMENT '审核管理员id',
  `content` text COMMENT '内容',
  `favor` int(11) DEFAULT '0' COMMENT '喜欢、点赞',
  PRIMARY KEY (`archive_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_archive` */

insert  into `tbl_archive`(`archive_id`,`post_type`,`title`,`member_id`,`create_time`,`description`,`keywords`,`view_rank`,`view_count`,`writer`,`source`,`pub_time`,`update_time`,`thumbnail`,`last_reply`,`can_reply`,`good_num`,`bad_num`,`check_admin`,`content`,`favor`) values (1,0,'?',1,'2017-11-27 19:07:14','?',NULL,0,1,'df','??','2017-11-27 19:07:14',NULL,NULL,NULL,0,0,0,0,'<p>?</p>\n',0),(2,0,'elasticsearch中文分词',1,'2018-01-09 09:46:13','ES的分词相关知识点',NULL,0,1,'','','2018-01-09 09:46:13',NULL,NULL,NULL,0,0,0,0,'<p>由于elasticsearch基于lucene，所以天然地就多了许多lucene上的中文分词的支持，比如 IK, Paoding, MMSEG4J等lucene中文分词原理上都能在elasticsearch上使用。当然前提是有elasticsearch的插件。 至于插件怎么开发，这里有一片文章介绍：<br />\nhttp://log.medcl.net/item/2011/07/diving-into-elasticsearch-3-custom-analysis-plugin/<br />\n暂时还没时间看，留在以后仔细研究， 这里只记录本人使用medcl提供的IK分词插件的集成步骤。<br />\n<br />\n一、插件准备<br />\n网上有介绍说可以直接用plugin -install medcl/elasticsearch-analysis-ik的办法，但是我执行下来的效果只是将插件的源码下载下来，elasticsearch只是将其作为一个_site插件看待。<br />\n所以只有执行maven并将打包后的jar文件拷贝到上级目录。（否则在定义mapping的analyzer的时候会提示找不到类的错误）。<br />\n由于IK是基于字典的分词，所以还要下载IK的字典文件，在medcl的elasticsearch-RTF中有，可以通过这个地址下载：<br />\nhttp://github.com/downloads/medcl/elasticsearch-analysis-ik/ik.zip<br />\n下载之后解压缩到config目录下。到这里，你可能需要重新启动下elasticsearch，好让下一部定义的分词器能立即生效。<br />\n<br />\n二、分词定义<br />\n分词插件准备好之后就可以在elasticsearch里定义（声明）这个分词类型了（自带的几个类型，比如standred则不需要特别定义）。跟其他设置一样，分词的定义也可以在系统级（elasticsearch全局范围），也可以在索引级（只在当前index内部可见）。系统级的定义当然是指在conf目录下的<br />\nelasticsearch.yml文件里定义，内容大致如下：<br />\nindex: &nbsp;<br />\n&nbsp; analysis: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />\n&nbsp; &nbsp; analyzer: &nbsp; &nbsp; &nbsp; &nbsp;<br />\n&nbsp; &nbsp; &nbsp; ikAnalyzer: &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; alias: [ik] &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: org.elasticsearch.index.analysis.IkAnalyzerProvider&nbsp;<br />\n或者 index.analysis.analyzer.ik.type : &quot;ik&quot;<br />\n<br />\n因为个人喜好，我并没有这么做， 而是定义在了需要使用中文分词的index中，这样定义更灵活，也不会影响其他index。<br />\n在定义analyze之前，先关闭index。其实并不需要关闭也可以生效，但是为了数据一致性考虑，还是先执行关闭。（如果是线上的系统需要三思）<br />\n<br />\ncurl -XPOST http://localhost:9400/application/_close<br />\n(很显然，这里的application是我的一个index）<br />\n<br />\n然后执行：<br />\ncurl -XPUT localhost:9400/application/_settings -d &#39;<br />\n{<br />\n&nbsp; &nbsp;&quot;analysis&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &quot;analyzer&quot;:{<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&quot;ikAnalyzer&quot;:{<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&quot;type&quot;:&quot;org.elasticsearch.index.analysis.IkAnalyzerProvider&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;alias&quot;:&quot;ik&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp;}<br />\n}<br />\n&#39;<br />\n打开index：<br />\n<br />\ncurl -XPOST http://localhost:9400/application/_open<br />\n<br />\n到此为止一个新的类型的分词器就定义好了，接下来就是要如何使用了<br />\n<br />\n或者按如下配置<br />\ncurl -XPUT localhost:9200/indexname -d &#39;{<br />\n&nbsp; &nbsp; &quot;settings&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;analysis&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;ik&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;tokenizer&quot; : &quot;ik&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &quot;mappings&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;article&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;dynamic&quot; : true,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;properties&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;title&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot; : &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot; : &quot;ik&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n}&#39;<br />\n如果我们想返回最细粒度的分词结果，需要在elasticsearch.yml中配置如下：<br />\n<br />\nindex:<br />\n&nbsp; analysis:<br />\n&nbsp; &nbsp; analyzer:<br />\n&nbsp; &nbsp; &nbsp; ik:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; alias: [ik_analyzer]<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: org.elasticsearch.index.analysis.IkAnalyzerProvider<br />\n&nbsp; &nbsp; &nbsp; ik_smart:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: true<br />\n&nbsp; &nbsp; &nbsp; ik_max_word:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: false<br />\n<br />\n三、使用分词器<br />\n在将分词器使用到实际数据之前，可以先测验下分词效果：<br />\nhttp://localhost:9400/application/_analyze?analyzer=ik&amp;text=中文分词<br />\n分词结果是：<br />\n{<br />\n&nbsp; &quot;tokens&quot; : [ {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;中文&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 0,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 2,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;CN_WORD&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 1<br />\n&nbsp; }, {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;分词&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 2,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 4,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;CN_WORD&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 2<br />\n&nbsp; } ]<br />\n}<br />\n与使用standard分词器的效果更合理了：<br />\n{<br />\n&nbsp; &quot;tokens&quot; : [ {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;中&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 0,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 1,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;&lt;IDEOGRAPHIC&gt;&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 1<br />\n&nbsp; }, {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;文&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 1,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 2,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;&lt;IDEOGRAPHIC&gt;&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 2<br />\n&nbsp; }, {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;分&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 2,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 3,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;&lt;IDEOGRAPHIC&gt;&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 3<br />\n&nbsp; }, {<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;词&quot;,<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 3,<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 4,<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;&lt;IDEOGRAPHIC&gt;&quot;,<br />\n&nbsp; &nbsp; &quot;position&quot; : 4<br />\n&nbsp; } ]<br />\n}<br />\n新的分词器定义完成，工作正常后就可以在mapping的定义中引用了，比如我定义这样的type：<br />\ncurl localhost:9400/application/article/_mapping -d &#39;<br />\n{<br />\n&nbsp; &nbsp; &quot;article&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;properties&quot;: { &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;description&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;:&quot;ikAnalyzer&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;:&quot;ikAnalyzer&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;title&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;:&quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;:&quot;ik&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n}<br />\n&#39;<br />\n很遗憾，对于已经存在的index来说，要将一个string类型的field从standard的分词器改成别的分词器通常都是失败的：<br />\n{<br />\n&nbsp; &nbsp; &quot;error&quot;: &quot;MergeMappingException[Merge failed with failures {[mapper [description] has different index_analyzer, mapper [description] has<br />\ndifferent search_analyzer]}]&quot;,<br />\n&nbsp; &nbsp; &quot;status&quot;: 400<br />\n}<br />\n而且没有办法解决冲突，唯一的办法是新建一个索引，并制定mapping使用新的分词器（注意要在数据插入之前，否则会使用elasticsearch默认的分词器）<br />\ncurl -XPUT localhost:9400/application/article/_mapping -d &#39;<br />\n{<br />\n&nbsp; &quot;article&quot; : {<br />\n&nbsp; &nbsp; &quot;properties&quot; : {<br />\n&nbsp;&quot;description&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;:&quot;ikAnalyzer&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;:&quot;ikAnalyzer&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;title&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;:&quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;:&quot;ik&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n&nbsp; }<br />\n}<br />\n至此，一个带中文分词的elasticsearch就算搭建完成。 想偷懒的可以下载medcl的elasticsearch-RTF直接使用，里面需要的插件和配置基本都已经设置好。<br />\n------------<br />\n标准分词（standard）配置如下：<br />\n<br />\ncurl -XPUT localhost:9200/local -d &#39;{<br />\n&nbsp; &nbsp; &quot;settings&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;analysis&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;stem&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;tokenizer&quot; : &quot;standard&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;filter&quot; : [&quot;standard&quot;, &quot;lowercase&quot;, &quot;stop&quot;, &quot;porter_stem&quot;]<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &quot;mappings&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;article&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;dynamic&quot; : true,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;properties&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;title&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot; : &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot; : &quot;stem&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n}&#39;<br />\n<br />\nindex:local<br />\ntype:article<br />\ndefault analyzer:stem (filter:小写、停用词等)<br />\nfield:title　　<br />\n测试：<br />\n<br />\n<br />\n# Sample Analysis<br />\ncurl -XGET localhost:9200/local/_analyze?analyzer=stem -d &#39;{Fight for your life}&#39;<br />\ncurl -XGET localhost:9200/local/_analyze?analyzer=stem -d &#39;{Bruno fights Tyson tomorrow}&#39;<br />\n&nbsp;<br />\n# Index Data<br />\ncurl -XPUT localhost:9200/local/article/1 -d&#39;{&quot;title&quot;: &quot;Fight for your life&quot;}&#39;<br />\ncurl -XPUT localhost:9200/local/article/2 -d&#39;{&quot;title&quot;: &quot;Fighting for your life&quot;}&#39;<br />\ncurl -XPUT localhost:9200/local/article/3 -d&#39;{&quot;title&quot;: &quot;My dad fought a dog&quot;}&#39;<br />\ncurl -XPUT localhost:9200/local/article/4 -d&#39;{&quot;title&quot;: &quot;Bruno fights Tyson tomorrow&quot;}&#39;<br />\n&nbsp;<br />\n# search on the title field, which is stemmed on index and search<br />\ncurl -XGET localhost:9200/local/_search?q=title:fight<br />\n&nbsp;<br />\n# searching on _all will not do anystemming, unless also configured on the mapping to be stemmed...<br />\ncurl -XGET localhost:9200/local/_search?q=fight<br />\n<br />\n例如：<br />\n<br />\nFight for your life<br />\n<br />\n分词如下：<br />\n<br />\n{&quot;tokens&quot;:[<br />\n{&quot;token&quot;:&quot;fight&quot;,&quot;start_offset&quot;:1,&quot;end_offset&quot;:6,&quot;type&quot;:&quot;&lt;ALPHANUM&gt;&quot;,&quot;position&quot;:1},&lt;br&gt;<br />\n{&quot;token&quot;:&quot;your&quot;,&quot;start_offset&quot;:11,&quot;end_offset&quot;:15,&quot;type&quot;:&quot;&lt;ALPHANUM&gt;&quot;,&quot;position&quot;:3},&lt;br&gt;<br />\n{&quot;token&quot;:&quot;life&quot;,&quot;start_offset&quot;:16,&quot;end_offset&quot;:20,&quot;type&quot;:&quot;&lt;ALPHANUM&gt;&quot;,&quot;position&quot;:4}<br />\n]}<br />\n<br />\n&nbsp;-------------------另一篇--------------------<br />\nElasticSearch安装ik分词插件<br />\n<br />\n一、IK简介<br />\n&nbsp; &nbsp; IK Analyzer是一个开源的，基于java语言开发的轻量级的中文分词工具包。从2006年12月推出1.0版开始， IKAnalyzer已经推出了4个大版本。最初，它是以开源项目Luence为应用主体的，结合词典分词和文法分析算法的中文分词组件。从3.0版本开 始，IK发展为面向Java的公用分词组件，独立于Lucene项目，同时提供了对Lucene的默认优化实现。在2012版本中，IK实现了简单的分词 歧义排除算法，标志着IK分词器从单纯的词典分词向模拟语义分词衍化。&nbsp;<br />\n&nbsp; &nbsp; IK Analyzer 2012特性:<br />\n&nbsp; &nbsp; 1.采用了特有的&ldquo;正向迭代最细粒度切分算法&ldquo;，支持细粒度和智能分词两种切分模式；<br />\n&nbsp; &nbsp; 2.在系统环境：Core2 i7 3.4G双核，4G内存，window 7 64位， Sun JDK 1.6_29 64位 普通pc环境测试，IK2012具有160万字/秒（3000KB/S）的高速处理能力。<br />\n&nbsp; &nbsp; 3.2012版本的智能分词模式支持简单的分词排歧义处理和数量词合并输出。<br />\n&nbsp; &nbsp; 4.采用了多子处理器分析模式，支持：英文字母、数字、中文词汇等分词处理，兼容韩文、日文字符<br />\n&nbsp; &nbsp; 5.优化的词典存储，更小的内存占用。支持用户词典扩展定义。特别的，在2012版本，词典支持中文，英文，数字混合词语。<br />\n<br />\n<br />\n二、安装IK分词插件<br />\n&nbsp; &nbsp; 假设读者已经安装好ES，如果没有的话，请参考ElasticSearch入门 &mdash;&mdash; 集群搭建。安装IK分词需要的资源可以从这里下载，整个安装过程需要三个步骤：<br />\n&nbsp; &nbsp; 1、获取分词的依赖包<br />\n&nbsp; &nbsp; 通过git clone https://github.com/medcl/elasticsearch-analysis-ik，下载分词器源码，然后进入下载目录，执行命令：mvn clean package，打包生成elasticsearch-analysis-ik-1.2.5.jar。将这个jar拷贝到ES_HOME/plugins/analysis-ik目录下面，如果没有该目录，则先创建该目录。<br />\n&nbsp; &nbsp; 2、ik目录拷贝<br />\n&nbsp; &nbsp; 将下载目录中的ik目录拷贝到ES_HOME/config目录下面。<br />\n&nbsp; &nbsp; 3、分词器配置<br />\n&nbsp; &nbsp; 打开ES_HOME/config/elasticsearch.yml文件，在文件最后加入如下内容：<br />\n<br />\nindex: &nbsp;<br />\n&nbsp; analysis: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />\n&nbsp; &nbsp; analyzer: &nbsp; &nbsp; &nbsp; &nbsp;<br />\n&nbsp; &nbsp; &nbsp; ik: &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; alias: [ik_analyzer] &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: org.elasticsearch.index.analysis.IkAnalyzerProvider &nbsp;<br />\n&nbsp; &nbsp; &nbsp; ik_max_word: &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: false &nbsp;<br />\n&nbsp; &nbsp; &nbsp; ik_smart: &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: true &nbsp;</p>\n\n<p>或<br />\nindex.analysis.analyzer.default.type: ik&nbsp;<br />\n<br />\n&nbsp; &nbsp; ok！插件安装已经完成，请重新启动ES，接下来测试ik分词效果啦！<br />\n三、ik分词测试<br />\n&nbsp; &nbsp; 1、创建一个索引，名为index。 &nbsp;<br />\n&nbsp;</p>\n\n<p>&nbsp; &nbsp; &nbsp;&nbsp;curl -XPUT http://localhost:9200/index&nbsp;<br />\n<br />\n&nbsp; &nbsp; 2、为索引index创建mapping。<br />\n<br />\ncurl -XPOST http://localhost:9200/index/fulltext/_mapping -d&#39; &nbsp;<br />\n{ &nbsp;<br />\n&nbsp; &nbsp; &quot;fulltext&quot;: { &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&quot;_all&quot;: { &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot;: &quot;ik&quot; &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }, &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;properties&quot;: { &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: { &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot; : &quot;string&quot;, &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;boost&quot; : 8.0, &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;term_vector&quot; : &quot;with_positions_offsets&quot;, &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;analyzer&quot; : &quot;ik&quot;, &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;include_in_all&quot; : true &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; } &nbsp;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; } &nbsp;<br />\n&nbsp; &nbsp; } &nbsp;<br />\n}&#39;&nbsp;<br />\n<br />\n&nbsp; &nbsp; 3、测试<br />\n<br />\ncurl &#39;http://localhost:9200/index/_analyze?analyzer=ik&amp;pretty=true&#39; -d &#39; &nbsp;<br />\n{ &nbsp;<br />\n&quot;text&quot;:&quot;世界如此之大&quot; &nbsp;<br />\n}&#39;&nbsp;<br />\n<br />\n&nbsp; &nbsp; 显示结果如下：<br />\n{ &nbsp;<br />\n&nbsp; &quot;tokens&quot; : [ { &nbsp;<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;text&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 4, &nbsp;<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 8, &nbsp;<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;ENGLISH&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;position&quot; : 1 &nbsp;<br />\n&nbsp; }, { &nbsp;<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;世界&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 11, &nbsp;<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 13, &nbsp;<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;CN_WORD&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;position&quot; : 2 &nbsp;<br />\n&nbsp; }, { &nbsp;<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;如此&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 13, &nbsp;<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 15, &nbsp;<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;CN_WORD&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;position&quot; : 3 &nbsp;<br />\n&nbsp; }, { &nbsp;<br />\n&nbsp; &nbsp; &quot;token&quot; : &quot;之大&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;start_offset&quot; : 15, &nbsp;<br />\n&nbsp; &nbsp; &quot;end_offset&quot; : 17, &nbsp;<br />\n&nbsp; &nbsp; &quot;type&quot; : &quot;CN_WORD&quot;, &nbsp;<br />\n&nbsp; &nbsp; &quot;position&quot; : 4 &nbsp;<br />\n&nbsp; } ] &nbsp;<br />\n}&nbsp;<br />\n----一下摘自官方----<br />\nDict Configuration (es-root/config/ik/IKAnalyzer.cfg.xml)<br />\n-&mdash;&mdash;&mdash;&mdash;&mdash;<br />\n<br />\nhttps://github.com/medcl/elasticsearch-analysis-ik/blob/master/config/ik/IKAnalyzer.cfg.xml<br />\n<br />\n﻿&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;<br />\n&lt;!DOCTYPE properties SYSTEM &quot;http://java.sun.com/dtd/properties.dtd&quot;&gt; &nbsp;<br />\n&lt;properties&gt; &nbsp;<br />\n&lt;comment&gt;IK Analyzer 扩展配置&lt;/comment&gt;<br />\n&lt;!--用户可以在这里配置自己的扩展字典 --&gt;<br />\n&lt;entry key=&quot;ext_dict&quot;&gt;custom/mydict.dic;custom/single_word_low_freq.dic&lt;/entry&gt;<br />\n&lt;!--用户可以在这里配置自己的扩展停止词字典--&gt;<br />\n&lt;entry key=&quot;ext_stopwords&quot;&gt;custom/ext_stopword.dic&lt;/entry&gt;&nbsp;<br />\n&nbsp; &lt;!--用户可以在这里配置远程扩展字典 --&gt;<br />\n&lt;entry key=&quot;remote_ext_dict&quot;&gt;location&lt;/entry&gt;&nbsp;<br />\n&nbsp; &lt;!--用户可以在这里配置远程扩展停止词字典--&gt;<br />\n&lt;entry key=&quot;remote_ext_stopwords&quot;&gt;location&lt;/entry&gt;&nbsp;<br />\n&lt;/properties&gt;<br />\n<br />\nAnalysis Configuration (elasticsearch.yml)<br />\n-&mdash;&mdash;&mdash;&mdash;&mdash;<br />\n<br />\nindex:<br />\n&nbsp; analysis: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />\n&nbsp; &nbsp; analyzer: &nbsp; &nbsp; &nbsp;<br />\n&nbsp; &nbsp; &nbsp; ik:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; alias: [ik_analyzer]<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: org.elasticsearch.index.analysis.IkAnalyzerProvider<br />\n&nbsp; &nbsp; &nbsp; ik_max_word:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: false<br />\n&nbsp; &nbsp; &nbsp; ik_smart:<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; type: ik<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; use_smart: true<br />\n<br />\nOr<br />\n<br />\nindex.analysis.analyzer.ik.type : &ldquo;ik&rdquo;<br />\n<br />\nyou can set your prefer segment mode,default `use_smart` is false.<br />\n<br />\nMapping Configuration<br />\n-&mdash;&mdash;&mdash;&mdash;&mdash;<br />\n<br />\nHere is a quick example:<br />\n1.create a index<br />\n<br />\ncurl -XPUT http://localhost:9200/index<br />\n<br />\n2.create a mapping<br />\n<br />\ncurl -XPOST http://localhost:9200/index/fulltext/_mapping -d&#39;<br />\n{<br />\n&nbsp; &nbsp; &quot;fulltext&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&quot;_all&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;: &quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;: &quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;term_vector&quot;: &quot;no&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;store&quot;: &quot;false&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;properties&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;string&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;store&quot;: &quot;no&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;term_vector&quot;: &quot;with_positions_offsets&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;indexAnalyzer&quot;: &quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;searchAnalyzer&quot;: &quot;ik&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;include_in_all&quot;: &quot;true&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;boost&quot;: 8<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n}&#39;<br />\n3.index some docs<br />\n<br />\ncurl -XPOST http://localhost:9200/index/fulltext/1 -d&#39;<br />\n{&quot;content&quot;:&quot;美国留给伊拉克的是个烂摊子吗&quot;}<br />\n&#39;<br />\ncurl -XPOST http://localhost:9200/index/fulltext/2 -d&#39;<br />\n{&quot;content&quot;:&quot;公安部：各地校车将享最高路权&quot;}<br />\n&#39;<br />\ncurl -XPOST http://localhost:9200/index/fulltext/3 -d&#39;<br />\n{&quot;content&quot;:&quot;中韩渔警冲突调查：韩警平均每天扣1艘中国渔船&quot;}<br />\n&#39;<br />\ncurl -XPOST http://localhost:9200/index/fulltext/4 -d&#39;<br />\n{&quot;content&quot;:&quot;中国驻洛杉矶领事馆遭亚裔男子枪击 嫌犯已自首&quot;}<br />\n&#39;<br />\n4.query with highlighting<br />\n<br />\ncurl -XPOST http://localhost:9200/index/fulltext/_search &nbsp;-d&#39;<br />\n{<br />\n&nbsp; &nbsp; &quot;query&quot; : { &quot;term&quot; : { &quot;content&quot; : &quot;中国&quot; }},<br />\n&nbsp; &nbsp; &quot;highlight&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;pre_tags&quot; : [&quot;&lt;tag1&gt;&quot;, &quot;&lt;tag2&gt;&quot;],<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;post_tags&quot; : [&quot;&lt;/tag1&gt;&quot;, &quot;&lt;/tag2&gt;&quot;],<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;fields&quot; : {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot; : {}<br />\n&nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; }<br />\n}<br />\n&#39;<br />\nhere is the query result<br />\n<br />\n{<br />\n&nbsp; &nbsp; &quot;took&quot;: 14,<br />\n&nbsp; &nbsp; &quot;timed_out&quot;: false,<br />\n&nbsp; &nbsp; &quot;_shards&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;total&quot;: 5,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;successful&quot;: 5,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;failed&quot;: 0<br />\n&nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &quot;hits&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;total&quot;: 2,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;max_score&quot;: 2,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &quot;hits&quot;: [<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_index&quot;: &quot;index&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_type&quot;: &quot;fulltext&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_id&quot;: &quot;4&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_score&quot;: 2,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_source&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: &quot;中国驻洛杉矶领事馆遭亚裔男子枪击 嫌犯已自首&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;highlight&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: [<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;&lt;tag1&gt;中国&lt;/tag1&gt;驻洛杉矶领事馆遭亚裔男子枪击 嫌犯已自首 &quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_index&quot;: &quot;index&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_type&quot;: &quot;fulltext&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_id&quot;: &quot;3&quot;,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_score&quot;: 2,<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;_source&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: &quot;中韩渔警冲突调查：韩警平均每天扣1艘中国渔船&quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; },<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;highlight&quot;: {<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;content&quot;: [<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;均每天扣1艘&lt;tag1&gt;中国&lt;/tag1&gt;渔船 &quot;<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }<br />\n&nbsp; &nbsp; &nbsp; &nbsp; ]<br />\n&nbsp; &nbsp; }<br />\n}</p>\n',0);

/*Table structure for table `tbl_archive_favor` */

DROP TABLE IF EXISTS `tbl_archive_favor`;

CREATE TABLE `tbl_archive_favor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `archive_id` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_archive_id_member_id` (`archive_id`,`member_id`),
  KEY `fk_archive_favor_member` (`member_id`),
  CONSTRAINT `fk_archive_favor_archive` FOREIGN KEY (`archive_id`) REFERENCES `tbl_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_archive_favor_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_archive_favor` */

/*Table structure for table `tbl_article` */

DROP TABLE IF EXISTS `tbl_article`;

CREATE TABLE `tbl_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collect_time` datetime DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL COMMENT '栏目ID',
  `archive_id` int(11) DEFAULT NULL COMMENT '文章ID',
  `status` int(11) DEFAULT '0' COMMENT '状态，0未审核，1已审核',
  PRIMARY KEY (`id`),
  KEY `fk_article_archive` (`archive_id`),
  KEY `fk_article_cate` (`cate_id`),
  CONSTRAINT `fk_article_archive` FOREIGN KEY (`archive_id`) REFERENCES `tbl_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_cate` FOREIGN KEY (`cate_id`) REFERENCES `tbl_article_cate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_article` */

insert  into `tbl_article`(`id`,`collect_time`,`cate_id`,`archive_id`,`status`) values (2,'2018-01-09 09:46:13',2,2,1);

/*Table structure for table `tbl_article_cate` */

DROP TABLE IF EXISTS `tbl_article_cate`;

CREATE TABLE `tbl_article_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) DEFAULT '0' COMMENT '上级类目ID，顶级栏目为0',
  `name` varchar(30) DEFAULT NULL COMMENT '栏目名称',
  `status` int(1) DEFAULT '0' COMMENT '0正常，1隐藏',
  `sort` int(11) DEFAULT '50' COMMENT '排序，越大越靠前',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_article_cate` */

insert  into `tbl_article_cate`(`id`,`fid`,`name`,`status`,`sort`) values (2,0,'NLP',0,50);

/*Table structure for table `tbl_article_comment` */

DROP TABLE IF EXISTS `tbl_article_comment`;

CREATE TABLE `tbl_article_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `fk_article_comment_member` (`member_id`),
  KEY `fk_article_comment_article` (`article_id`),
  CONSTRAINT `fk_article_comment_article` FOREIGN KEY (`article_id`) REFERENCES `tbl_article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_comment_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_article_comment` */

/*Table structure for table `tbl_config` */

DROP TABLE IF EXISTS `tbl_config`;

CREATE TABLE `tbl_config` (
  `jkey` varchar(100) NOT NULL DEFAULT '',
  `jvalue` varchar(500) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  PRIMARY KEY (`jkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_config` */

insert  into `tbl_config`(`jkey`,`jvalue`,`description`) values ('cms_post','1','cms会员文章投稿，0关闭，1开启'),('cms_post_review','0','cms投稿审核，0需要审核，1不需要审核'),('group_alias','群组','群组别名'),('group_apply','1','群组是否可以申请，0不可以，1可以'),('group_apply_review','0','群组申请是否需要审核，0需要审核，1不需要审核'),('member_email_valid','0','邮箱验证，0不需要验证，1需要验证'),('member_login_open','1','会员登录开关，0关闭，1开启'),('member_register_open','1','会员注册开关，0关闭，1开启'),('site_copyright','Copyright © 2017 - 2018.','版权说明'),('site_description','自然语言处理','网站描述'),('site_domain','http://www.datatruth.cn/','网站域名'),('site_icp','','备案号'),('site_keys','自然语言处理，Java，Spring，机器学习，搜索引擎','网站关键词'),('site_logo','/res/common/images/lxinetlogo.png','网站LOGO'),('site_name','DTSNS','网站名称'),('site_send_email_account','','发送邮箱账号'),('site_send_email_password','','发送邮箱密码'),('site_send_email_smtp','','发送邮箱SMTP服务器地址'),('site_seo_title','DTSNS社区','SEO标题'),('site_tongji','<script>var _hmt = _hmt || [];(function() {var hm = document.createElement(\"script\");hm.src = \"https://hm.baidu.com/hm.js?6e79d941db914e4195f4a839b06f2567\";var s = document.getElementsByTagName(\"script\")[0]; s.parentNode.insertBefore(hm, s);})();</script>','统计代码'),('weibo_alias','微博','微博别名'),('weibo_post','1','微博发布，0不可以发布，1可以发布'),('weibo_post_maxcontent','140','微博内容字数');

/*Table structure for table `tbl_group` */

DROP TABLE IF EXISTS `tbl_group`;

CREATE TABLE `tbl_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '群组名字',
  `logo` varchar(255) DEFAULT NULL COMMENT '群组logo',
  `creator` int(11) DEFAULT NULL COMMENT '创建人',
  `managers` varchar(200) DEFAULT NULL COMMENT '管理员',
  `tags` varchar(100) DEFAULT NULL COMMENT '标签',
  `introduce` varchar(255) DEFAULT NULL COMMENT '介绍',
  `can_post` int(11) DEFAULT '0' COMMENT '是否能发帖，0不可以，1可以',
  `topic_review` int(11) DEFAULT '0' COMMENT '帖子是否需要审核，0不需要，1需要',
  `status` int(11) DEFAULT '0' COMMENT '0未审核，1已审核，-1审核不通过',
  PRIMARY KEY (`id`),
  KEY `fk_group_member` (`creator`),
  CONSTRAINT `fk_group_member` FOREIGN KEY (`creator`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_group` */

/*Table structure for table `tbl_group_fans` */

DROP TABLE IF EXISTS `tbl_group_fans`;

CREATE TABLE `tbl_group_fans` (
  `create_time` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  UNIQUE KEY `uk_group_id_member_id` (`group_id`,`member_id`),
  KEY `fk_group_fans_member` (`member_id`),
  CONSTRAINT `fk_group_fans_group` FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_group_fans_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_group_fans` */

/*Table structure for table `tbl_group_topic` */

DROP TABLE IF EXISTS `tbl_group_topic`;

CREATE TABLE `tbl_group_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collect_time` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `archive_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '状态，0未审核，1已审核',
  `is_essence` int(11) DEFAULT '0' COMMENT '精华，0不加精，1加精',
  `is_top` int(11) DEFAULT '0' COMMENT '置顶，0不置顶，1置顶，2超级置顶',
  PRIMARY KEY (`id`),
  KEY `fk_group_topic_group` (`group_id`),
  KEY `fk_group_topic_archive` (`archive_id`),
  CONSTRAINT `fk_group_topic_archive` FOREIGN KEY (`archive_id`) REFERENCES `tbl_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_group_topic_group` FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_group_topic` */

/*Table structure for table `tbl_group_topic_comment` */

DROP TABLE IF EXISTS `tbl_group_topic_comment`;

CREATE TABLE `tbl_group_topic_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `group_topic_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `fk_group_topic_comment_member` (`member_id`),
  KEY `fk_group_topic_comment_group_topic` (`group_topic_id`),
  CONSTRAINT `fk_group_topic_comment_group_topic` FOREIGN KEY (`group_topic_id`) REFERENCES `tbl_group_topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_group_topic_comment_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_group_topic_comment` */

/*Table structure for table `tbl_link` */

DROP TABLE IF EXISTS `tbl_link`;

CREATE TABLE `tbl_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '网站名称',
  `url` varchar(255) DEFAULT NULL COMMENT '网址',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，越大越靠前',
  `recomment` int(11) NOT NULL DEFAULT '0' COMMENT '推荐，0不推荐，1推荐',
  `status` int(1) DEFAULT '0' COMMENT '状态，0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_link` */

/*Table structure for table `tbl_member` */

DROP TABLE IF EXISTS `tbl_member`;

CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT '0' COMMENT '分组ID',
  `name` varchar(50) DEFAULT NULL COMMENT '会员名称',
  `email` varchar(50) DEFAULT '' COMMENT '邮箱',
  `phone` varchar(11) DEFAULT '' COMMENT '手机号码',
  `password` varchar(32) DEFAULT '' COMMENT '密码',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `regip` varchar(15) DEFAULT '' COMMENT '注册IP',
  `login_count` int(11) DEFAULT '0' COMMENT '登录次数',
  `curr_login_time` datetime DEFAULT NULL COMMENT '本次登录时间',
  `curr_login_ip` varchar(15) DEFAULT NULL COMMENT '本次登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(15) DEFAULT NULL COMMENT '上次登录IP',
  `update_time` datetime DEFAULT NULL COMMENT '更新资料时间',
  `money` double(11,2) DEFAULT '0.00' COMMENT '金额',
  `score` int(11) DEFAULT '0' COMMENT '积分',
  `is_active` int(1) DEFAULT '0' COMMENT '是否已激活，0未激活，1已激活',
  `status` int(2) DEFAULT '0' COMMENT '-1禁用，0启用',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `addprovince` varchar(20) DEFAULT '' COMMENT '居住省份',
  `addcity` varchar(20) DEFAULT '' COMMENT '居住城市',
  `addarea` varchar(20) DEFAULT '' COMMENT '居住地区',
  `address` varchar(50) DEFAULT '' COMMENT '居住地址',
  `qq` varchar(15) DEFAULT '' COMMENT 'QQ',
  `wechat` varchar(20) DEFAULT '' COMMENT '微信',
  `contact_phone` varchar(11) DEFAULT '' COMMENT '联系手机号',
  `contact_email` varchar(32) DEFAULT '' COMMENT '联系邮箱',
  `website` varchar(50) DEFAULT '' COMMENT '个人网站',
  `introduce` varchar(255) DEFAULT '' COMMENT '个人介绍',
  `is_admin` int(11) DEFAULT '0' COMMENT '是否管理员，0不是，1是普通管理员，2是超级管理员',
  `follows` int(11) DEFAULT '0' COMMENT '关注会员数量',
  `fans` int(11) DEFAULT '0' COMMENT '粉丝数量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_member` */

insert  into `tbl_member`(`id`,`group_id`,`name`,`email`,`phone`,`password`,`sex`,`avatar`,`create_time`,`regip`,`login_count`,`curr_login_time`,`curr_login_ip`,`last_login_time`,`last_login_ip`,`update_time`,`money`,`score`,`is_active`,`status`,`birthday`,`addprovince`,`addcity`,`addarea`,`address`,`qq`,`wechat`,`contact_phone`,`contact_email`,`website`,`introduce`,`is_admin`,`follows`,`fans`) values (1,0,'admin','admin@dtsns.cn','13800138000','e10adc3949ba59abbe56e057f20f883e','女','/res/common/images/default-avatar.png','2017-11-03 17:23:41','',7,'2018-03-12 09:35:29','0:0:0:0:0:0:0:1','2018-03-12 09:34:37','0:0:0:0:0:0:0:1',NULL,0.00,5,1,0,'1971-12-20','','','','','8888888','admin','13800138000','admin@dtsns.cn','www.dtsns.cn','',2,0,0);

/*Table structure for table `tbl_member_fans` */

DROP TABLE IF EXISTS `tbl_member_fans`;

CREATE TABLE `tbl_member_fans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `follow_who` int(11) DEFAULT '0',
  `who_follow` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_follow_who_who_follow` (`follow_who`,`who_follow`),
  KEY `fk_member_fans_who_follow` (`who_follow`),
  CONSTRAINT `fk_member_fans_follow_who` FOREIGN KEY (`follow_who`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_member_fans_who_follow` FOREIGN KEY (`who_follow`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_member_fans` */

/*Table structure for table `tbl_member_token` */

DROP TABLE IF EXISTS `tbl_member_token`;

CREATE TABLE `tbl_member_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT '0' COMMENT '会员ID',
  `token` varchar(32) DEFAULT '',
  `expire_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '状态，0是正常，1是失效',
  PRIMARY KEY (`id`),
  KEY `fk_member_token_member` (`member_id`),
  CONSTRAINT `fk_member_token_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_member_token` */

/*Table structure for table `tbl_memgroup` */

DROP TABLE IF EXISTS `tbl_memgroup`;

CREATE TABLE `tbl_memgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isadmin` int(1) DEFAULT '0' COMMENT '是否是管理组，0不是，1是',
  `name` varchar(50) DEFAULT '' COMMENT '分组名称',
  `fid` int(11) DEFAULT '0' COMMENT '上级分组ID，默认0，0是顶级分组',
  `rankid` int(11) DEFAULT '0' COMMENT '权限ID，0-99是会员权限，100以上是管理员权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_memgroup` */

/*Table structure for table `tbl_message` */

DROP TABLE IF EXISTS `tbl_message`;

CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `from_member_id` int(11) DEFAULT '0',
  `to_member_id` int(11) DEFAULT '0',
  `content` text,
  `url` varchar(255) DEFAULT NULL,
  `app_tag` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `relate_key_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `isread` int(1) DEFAULT '0' COMMENT '是否已读，0未读，1已读',
  PRIMARY KEY (`id`),
  KEY `fk_message_from_member` (`from_member_id`),
  KEY `fk_message_to_member` (`to_member_id`),
  KEY `fk_message_member` (`member_id`),
  CONSTRAINT `fk_message_from_member` FOREIGN KEY (`from_member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_message_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_message_to_member` FOREIGN KEY (`to_member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_message` */

/*Table structure for table `tbl_picture` */

DROP TABLE IF EXISTS `tbl_picture`;

CREATE TABLE `tbl_picture` (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1是文章图片，2是群组帖子图片，3是微博图片',
  `foreign_id` int(11) DEFAULT NULL COMMENT '外键ID',
  `path` varchar(255) NOT NULL COMMENT '图片路径',
  `thumbnail_path` varchar(255) DEFAULT NULL COMMENT '缩小的图片路径',
  `md5` varchar(32) NOT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`picture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_picture` */

/*Table structure for table `tbl_score_detail` */

DROP TABLE IF EXISTS `tbl_score_detail`;

CREATE TABLE `tbl_score_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT '0' COMMENT '会员ID',
  `type` int(11) DEFAULT '0' COMMENT '类型，0是普通积分增加，1是奖励，2是撤销奖励',
  `score` int(11) DEFAULT '0' COMMENT '变化积分',
  `balance` int(11) DEFAULT '0' COMMENT '账户剩余积分',
  `remark` varchar(255) DEFAULT NULL COMMENT '说明',
  `foreign_id` int(11) DEFAULT '0' COMMENT '外键ID',
  `score_rule_id` int(11) DEFAULT '0' COMMENT '积分规则ID',
  `status` int(11) DEFAULT '1' COMMENT '状态，1是成功，0是取消',
  PRIMARY KEY (`id`),
  KEY `fk_score_detail_member` (`member_id`),
  KEY `fk_score_detail_score_rule` (`score_rule_id`),
  CONSTRAINT `fk_score_detail_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_score_detail_score_rule` FOREIGN KEY (`score_rule_id`) REFERENCES `tbl_score_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_score_detail` */

insert  into `tbl_score_detail`(`id`,`create_time`,`member_id`,`type`,`score`,`balance`,`remark`,`foreign_id`,`score_rule_id`,`status`) values (1,'2017-11-27 18:47:56',1,1,1,1,'??????',0,3,1),(2,'2017-11-27 19:07:14',1,1,1,2,'???? #1',1,4,1),(3,'2018-01-08 17:37:23',1,1,1,3,'??????',0,3,1),(4,'2018-01-09 09:46:13',1,1,1,4,'文章投稿 #2',2,4,1),(5,'2018-03-12 09:35:29',1,1,1,5,'每天登陆奖励',0,3,1);

/*Table structure for table `tbl_score_rule` */

DROP TABLE IF EXISTS `tbl_score_rule`;

CREATE TABLE `tbl_score_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `name` varchar(30) DEFAULT '0' COMMENT '规则名称',
  `score` int(11) DEFAULT '0' COMMENT '变化积分',
  `remark` varchar(255) DEFAULT NULL COMMENT '说明',
  `type` varchar(10) DEFAULT 'unlimite' COMMENT '奖励次数类型，day每天一次，week每周一次，month每月一次，year每年一次，one只有一次，unlimite不限次数',
  `status` int(11) DEFAULT '1' COMMENT '状态，0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_score_rule` */

insert  into `tbl_score_rule`(`id`,`create_time`,`update_time`,`name`,`score`,`remark`,`type`,`status`) values (1,'2017-11-03 17:23:41','2017-11-03 17:23:41','注册奖励',100,'注册奖励','one',1),(2,'2017-11-03 17:23:41','2017-11-03 17:23:41','邮箱认证',1,'邮箱认证奖励积分，只有一次','one',1),(3,'2017-11-03 17:23:41','2017-11-03 17:23:41','每天登陆奖励',1,'每天登陆奖励积分，一天仅限一次','day',1),(4,'2017-11-03 17:23:41','2017-11-03 17:23:41','文章投稿',1,'文章投稿奖励积分，如需审核，审核之后奖励','unlimite',1),(5,'2017-11-03 17:23:41','2017-11-03 17:23:41','文章评论',1,'评论文章奖励积分','unlimite',1),(6,'2017-11-03 17:23:41','2017-11-03 17:23:41','文章收到喜欢',1,'文章收到喜欢，作者奖励积分','unlimite',1),(7,'2017-11-03 17:23:41','2017-11-03 17:23:41','发布微博',1,'发布微博奖励积分','unlimite',1),(8,'2017-11-03 17:23:41','2017-11-03 17:23:41','评论微博',1,'评论微博奖励积分','unlimite',1),(9,'2017-11-03 17:23:41','2017-11-03 17:23:41','微博收到点赞',1,'微博收到点赞，作者奖励积分','unlimite',1),(10,'2017-11-03 17:23:41','2017-11-03 17:23:41','申请群组',-10,'申请群组扣除/奖励积分，如需要扣除积分，请填写负数','unlimite',1),(11,'2017-11-03 17:23:41','2017-11-03 17:23:41','群组发帖',1,'群组发帖奖励积分，如需审核，审核之后奖励','unlimite',1),(12,'2017-11-03 17:23:41','2017-11-03 17:23:41','群组帖子评论',1,'群组帖子评论奖励积分','unlimite',1),(13,'2017-11-03 17:23:41','2017-11-03 17:23:41','群组帖子收到喜欢',1,'群组帖子收到喜欢奖励积分','unlimite',1);

/*Table structure for table `tbl_validate_code` */

DROP TABLE IF EXISTS `tbl_validate_code`;

CREATE TABLE `tbl_validate_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `email` varchar(32) NOT NULL DEFAULT '',
  `code` varchar(50) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '0',
  `type` int(1) DEFAULT '0' COMMENT '1是重置密码，2会员激活',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_validate_code` */

/*Table structure for table `tbl_weibo` */

DROP TABLE IF EXISTS `tbl_weibo`;

CREATE TABLE `tbl_weibo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `type` int(11) DEFAULT '0' COMMENT '0为普通文本,1为图片',
  `content` varchar(1000) DEFAULT NULL,
  `favor` int(11) DEFAULT '0' COMMENT '赞',
  `status` tinyint(11) DEFAULT '0' COMMENT '0未审核，1已审核，-1审核不通过',
  PRIMARY KEY (`id`),
  KEY `fk_weibo_member` (`member_id`),
  CONSTRAINT `fk_weibo_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_weibo` */

/*Table structure for table `tbl_weibo_comment` */

DROP TABLE IF EXISTS `tbl_weibo_comment`;

CREATE TABLE `tbl_weibo_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `member_id` int(11) NOT NULL DEFAULT '0',
  `weibo_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) DEFAULT NULL COMMENT '评论的id',
  `content` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0正常，1禁用',
  PRIMARY KEY (`id`),
  KEY `fk_weibo_comment_member` (`member_id`),
  KEY `fk_weibo_comment_weibo` (`weibo_id`),
  CONSTRAINT `fk_weibo_comment_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_weibo_comment_weibo` FOREIGN KEY (`weibo_id`) REFERENCES `tbl_weibo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_weibo_comment` */

/*Table structure for table `tbl_weibo_favor` */

DROP TABLE IF EXISTS `tbl_weibo_favor`;

CREATE TABLE `tbl_weibo_favor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `weibo_id` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_weibo_id_member_id` (`weibo_id`,`member_id`),
  KEY `fk_weibo_favor_member` (`member_id`),
  CONSTRAINT `fk_weibo_favor_member` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_weibo_favor_weibo` FOREIGN KEY (`weibo_id`) REFERENCES `tbl_weibo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_weibo_favor` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
