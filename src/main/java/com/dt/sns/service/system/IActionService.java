package com.dt.sns.service.system;

import java.util.List;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.system.Action;

/**
 * Created by zchuanzhao on 2017/2/14.
 */
public interface IActionService {

    List<Action> list();

    Action findById(Integer id);

    ResponseModel update(Action action);

    ResponseModel isenable(Integer id);

    boolean canuse(Integer id);
}
