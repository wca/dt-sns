package com.dt.sns.service.system;

import java.util.List;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.system.ScoreRule;

/**
 * Created by zchuanzhao on 2017/2/14.
 */
public interface IScoreRuleService {

    List<ScoreRule> list();

    ScoreRule findById(Integer id);

    ResponseModel update(ScoreRule scoreRule);

    ResponseModel enabled(int id);

}
