package com.dt.sns.service.system.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.mapper.system.IScoreRuleDao;
import com.dt.sns.model.system.ScoreRule;
import com.dt.sns.service.system.IScoreRuleService;

/**
 * Created by zchuanzhao on 2017/3/24.
 */
@Service("scoreRuleService")
public class ScoreRuleServiceImpl implements IScoreRuleService {
	@Resource
	private IScoreRuleDao scoreRuleDao;

	@Override
	public List<ScoreRule> list() {
		return scoreRuleDao.allList();
	}

	@Override
	public ScoreRule findById(Integer id) {
		return scoreRuleDao.findById(id);
	}

	@Override
	public ResponseModel update(ScoreRule scoreRule) {
		if (scoreRuleDao.update(scoreRule) == 1) {
			return new ResponseModel(0, "操作成功");
		}
		return new ResponseModel(-1, "操作失败");
	}

	@Override
	public ResponseModel enabled(int id) {
		if (scoreRuleDao.enabled(id) == 1) {
			return new ResponseModel(0, "操作成功");
		}
		return new ResponseModel(-1, "操作失败");
	}

}
