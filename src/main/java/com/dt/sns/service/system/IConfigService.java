package com.dt.sns.service.system;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.system.Config;

/**
 * Created by zchuanzhao on 16/9/29.
 */
public interface IConfigService {
	List<Config> allList();

	Map<String, String> getConfigToMap();

	String getValue(String key);

	ResponseModel<Config> update(Map<String, String> params, HttpServletRequest request);
}
