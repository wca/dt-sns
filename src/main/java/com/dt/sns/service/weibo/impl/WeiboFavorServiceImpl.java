package com.dt.sns.service.weibo.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dt.sns.mapper.weibo.IWeiboFavorDao;
import com.dt.sns.model.weibo.WeiboFavor;
import com.dt.sns.service.weibo.IWeiboFavorService;

/**
 * Created by zchuanzhao on 2017/2/8.
 */
@Service("weiboFavorService")
public class WeiboFavorServiceImpl implements IWeiboFavorService {
    @Resource
    private IWeiboFavorDao weiboFavorDao;


    @Override
    public WeiboFavor find(Integer weiboId, Integer memberId) {
        return weiboFavorDao.find(weiboId,memberId);
    }

    @Override
    public void save(Integer weiboId, Integer memberId) {
        weiboFavorDao.save(weiboId,memberId);
    }

    @Override
    public void delete(Integer weiboId, Integer memberId) {
        weiboFavorDao.delete(weiboId,memberId);
    }
}
