package com.dt.sns.service.weibo;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.member.Member;
import com.dt.sns.model.weibo.WeiboComment;


/**
 * Created by zchuanzhao on 2016/10/14.
 */
public interface IWeiboCommentService {

    WeiboComment findById(int id);

    ResponseModel save(Member loginMember, String content, Integer weiboId, Integer weiboCommentId);

    ResponseModel delete(Member loginMember,int id);

    ResponseModel listByWeibo(Page page, int articleId);

    void deleteByWeibo(Integer weiboId);
}
