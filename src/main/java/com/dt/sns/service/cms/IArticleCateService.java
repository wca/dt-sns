package com.dt.sns.service.cms;

import java.util.List;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.cms.ArticleCate;

/**
 * Created by zchuanzhao on 16/9/29.
 */
public interface IArticleCateService {

	ArticleCate findById(int id);

	int save(ArticleCate articleCate);

	int update(ArticleCate articleCate);

	ResponseModel delete(int id);

	/**
	 * 获取栏目
	 * 
	 * @return
	 */
	List<ArticleCate> list();

	/**
	 * 通过父类ID获取子类列表
	 * 
	 * @param fid
	 * @return
	 */
	List<ArticleCate> findListByFid(int fid);
}
