package com.dt.sns.service.cms;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.cms.ArticleComment;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.member.Member;


/**
 * Created by zchuanzhao on 2016/10/14.
 */
public interface IArticleCommentService {

    ArticleComment findById(int id);

    ResponseModel save(Member loginMember, String content, Integer articleId);

    ResponseModel delete(Member loginMember, int id);

    ResponseModel listByArticle(Page page, int articleId);

    void deleteByArticle(Integer articleId);
}
