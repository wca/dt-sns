package com.dt.sns.service.common;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Ads;
import com.dt.sns.model.common.Page;

/**
 * Created by MMF on 2017-09-07.
 */
public interface IAdsService {
	/**
	 * 保存广告信息
	 * 
	 * @param ads
	 * @return
	 */
	ResponseModel save(Ads ads);

	/**
	 * 分页查询广告信息
	 * 
	 * @param page
	 * @return
	 */
	ResponseModel listByPage(Page page);

	ResponseModel update(Ads ads);

	ResponseModel delete(Integer id);

	Ads findById(Integer id);

	ResponseModel enable(Integer id);
}
