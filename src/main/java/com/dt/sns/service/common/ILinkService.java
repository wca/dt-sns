package com.dt.sns.service.common;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Link;
import com.dt.sns.model.common.Page;

/**
 * Created by zchuanzhao on 2017-10-13.
 */
public interface ILinkService {
   
    ResponseModel save(Link link);
   
    ResponseModel listByPage(Page page);

    ResponseModel allList();

    ResponseModel recommentList();

    ResponseModel update(Link link);

    ResponseModel delete(Integer id);

    Link findById(Integer id);

    ResponseModel enable(Integer id);
}
