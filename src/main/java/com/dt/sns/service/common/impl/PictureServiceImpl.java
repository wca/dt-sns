package com.dt.sns.service.common.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.dt.sns.core.util.StringUtils;
import com.dt.sns.mapper.common.IPictureDao;
import com.dt.sns.model.common.Picture;
import com.dt.sns.service.common.IPictureService;
import com.dt.sns.util.PictureUtil;

/**
 * Created by zchuanzhao on 2017/3/7.
 */
@Service("pictureService")
public class PictureServiceImpl implements IPictureService {
    @Resource
    private IPictureDao pictureDao;

    @Override
    public List<Picture> find(Integer foreignId) {
        return pictureDao.find(foreignId);
    }

    @Override
    public int delete(HttpServletRequest request, Integer foreignId) {
        List<Picture> pictures = this.find(foreignId);
        PictureUtil.delete(request,pictures);
        return pictureDao.delete(foreignId);
    }

    @Override
    public int save(Picture picture) {
        return pictureDao.save(picture);
    }

    @Override
    public int update(Integer foreignId, String ids) {
        if(StringUtils.isNotEmpty(ids)){
            String[] idsArr = ids.split(",");
            return pictureDao.update(foreignId, idsArr);
        }
        return 0;
    }
}
