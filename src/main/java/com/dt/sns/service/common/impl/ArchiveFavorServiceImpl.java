package com.dt.sns.service.common.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dt.sns.mapper.common.IArchiveFavorDao;
import com.dt.sns.model.common.ArchiveFavor;
import com.dt.sns.service.common.IArchiveFavorService;

/**
 * Created by zchuanzhao on 2017/2/9.
 */
@Service("archiveFavorService")
public class ArchiveFavorServiceImpl implements IArchiveFavorService {
	@Resource
	private IArchiveFavorDao archiveFavorDao;

	@Override
	public ArchiveFavor find(Integer archiveId, Integer memberId) {
		return archiveFavorDao.find(archiveId, memberId);
	}

	@Override
	public void save(Integer archiveId, Integer memberId) {
		archiveFavorDao.save(archiveId, memberId);
	}

	@Override
	public void delete(Integer archiveId, Integer memberId) {
		archiveFavorDao.delete(archiveId, memberId);
	}
}
