package com.dt.sns.service.common;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dt.sns.mapper.common.ICommonDao;

/**
 * Created by zchuanzhao on 2017/2/6.
 */
@Service("commonService")
public class CommonServiceImpl implements ICommonService {
	@Resource
	private ICommonDao commonDao;

	@Override
	public String getMysqlVsesion() {
		return commonDao.getMysqlVsesion();
	}
}
