package com.dt.sns.service.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dt.sns.model.common.Picture;

/**
 * Created by zchuanzhao on 2017/3/7.
 */
public interface IPictureService {

	List<Picture> find(Integer foreignId);

	int delete(HttpServletRequest request, Integer foreignId);

	int save(Picture picture);

	int update(Integer foreignId, String ids);
}