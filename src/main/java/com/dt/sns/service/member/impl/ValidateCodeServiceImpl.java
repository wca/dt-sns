package com.dt.sns.service.member.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dt.sns.mapper.member.IValidateCodeDao;
import com.dt.sns.model.member.ValidateCode;
import com.dt.sns.service.member.IValidateCodeService;

/**
 * Created by zchuanzhao on 2017/1/20.
 */
@Service("validateCodeService")
public class ValidateCodeServiceImpl implements IValidateCodeService {
    @Resource
    private IValidateCodeDao validateCodeDao;

    @Override
    public boolean save(ValidateCode validateCode) {
        return validateCodeDao.save(validateCode) == 1;
    }

    @Override
    public ValidateCode valid(String email, String code, int type) {
        return validateCodeDao.valid(email,code,type);
    }

    @Override
    public boolean used(Integer id) {
        return validateCodeDao.used(id) == 1;
    }
}
