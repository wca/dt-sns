package com.dt.sns.service.member;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.member.MemberFans;

/**
 * Created by zchuanzhao on 17/2/21.
 */
public interface IMemberFansService {

	ResponseModel save(Integer whoFollowId, Integer followWhoId);

	ResponseModel delete(Integer whoFollowId, Integer followWhoId);

	ResponseModel followsList(Page page, Integer whoFollowId);

	ResponseModel fansList(Page page, Integer followWhoId);

	MemberFans find(Integer whoFollowId, Integer followWhoId);
}
