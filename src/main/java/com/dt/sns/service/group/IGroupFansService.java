package com.dt.sns.service.group;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.group.GroupFans;
import com.dt.sns.model.member.Member;


/**
 * Created by zchuanzhao on 16/12/26.
 */
public interface IGroupFansService {

    ResponseModel save(Member loginMember, Integer groupId);

    ResponseModel delete(Member loginMember, Integer groupId);

    ResponseModel listByPage(Page page, Integer groupId);

    GroupFans findByMemberAndGroup(@Param("groupId") Integer groupId, @Param("memberId") Integer memberId);

    /**
     * 获取用户关注的群组列表
     * @param page
     * @param memberId
     * @return
     */
    ResponseModel listByMember(Page page, Integer memberId);
}
