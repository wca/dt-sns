package com.dt.sns.service.group;

import java.util.List;

import com.dt.sns.core.ResponseModel;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.group.Group;
import com.dt.sns.model.member.Member;


/**
 * Created by zchuanzhao on 16/12/23.
 */
public interface IGroupService {

    Group findById(int id);

    ResponseModel save(Member loginMember, Group group);

    ResponseModel update(Member loginMember, Group group);

    ResponseModel delete(Member loginMember, int id);

    ResponseModel listByPage(int status, Page page, String key);

    ResponseModel follow(Member loginMember, Integer groupId,int type);

    ResponseModel changeStatus(int id);

    List<Group> listByCustom(int status, int num, String sort);
}
