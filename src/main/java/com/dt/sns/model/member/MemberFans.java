package com.dt.sns.model.member;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * 
 * Created by 岳海亮 on 2018年4月11日.
 */
public class MemberFans implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	private Integer followWho;
	private Member followWhoMember;
	private Integer whoFollow;
	private Member whoFollowMember;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getFollowWho() {
		return followWho;
	}

	public void setFollowWho(Integer followWho) {
		this.followWho = followWho;
	}

	public Member getFollowWhoMember() {
		return followWhoMember;
	}

	public void setFollowWhoMember(Member followWhoMember) {
		this.followWhoMember = followWhoMember;
	}

	public Integer getWhoFollow() {
		return whoFollow;
	}

	public void setWhoFollow(Integer whoFollow) {
		this.whoFollow = whoFollow;
	}

	public Member getWhoFollowMember() {
		return whoFollowMember;
	}

	public void setWhoFollowMember(Member whoFollowMember) {
		this.whoFollowMember = whoFollowMember;
	}
}