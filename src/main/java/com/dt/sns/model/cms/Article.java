package com.dt.sns.model.cms;

import java.util.Date;

import javax.validation.constraints.Digits;

import com.dt.sns.model.common.Archive;

/**
 * 文章实体类 Created by 岳海亮 on 2017年12月26日.
 */
public class Article extends Archive {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Date collectTime;
	@Digits(integer = 11, fraction = 0, message = "栏目不能为空")
	private Integer cateId;
	private Integer status;

	private ArticleCate articleCate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCollectTime() {
		return collectTime;
	}

	public void setCollectTime(Date collectTime) {
		this.collectTime = collectTime;
	}

	public Integer getCateId() {
		return cateId;
	}

	public void setCateId(Integer cateId) {
		this.cateId = cateId;
	}

	public ArticleCate getArticleCate() {
		return articleCate;
	}

	public void setArticleCate(ArticleCate articleCate) {
		this.articleCate = articleCate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}