package com.dt.sns.model.group;


import java.util.Date;

import com.dt.sns.model.common.Archive;

/**
 * Created by zchuanzhao on 16/12/26.
 */
public class GroupTopic extends Archive {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
    private Date collectTime;
    private Integer groupId;
    private Group group;
    private Integer status;
    private Integer isTop;
    private Integer isEssence;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(Date collectTime) {
        this.collectTime = collectTime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getIsEssence() {
        return isEssence;
    }

    public void setIsEssence(Integer isEssence) {
        this.isEssence = isEssence;
    }
}