package com.dt.sns.web.directive;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.dt.sns.core.BaseDirective;
import com.dt.sns.core.DirectiveHandler;
import com.dt.sns.model.common.Ads;
import com.dt.sns.service.common.IAdsService;

import freemarker.template.TemplateException;

/**
 * Created by zchuanzhao on 2017/09/08.
 */
@Component
public class AdsDirective extends BaseDirective {

    @Resource
    private IAdsService adsService;
    @Override
    public void execute(DirectiveHandler handler) throws TemplateException, IOException {
        int id = handler.getInteger("id",0);
        Ads ads = adsService.findById(id);
        handler.put("ad", ads).render();
    }

}
