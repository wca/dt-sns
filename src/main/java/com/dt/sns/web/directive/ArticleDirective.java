package com.dt.sns.web.directive;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.dt.sns.core.BaseDirective;
import com.dt.sns.core.DirectiveHandler;
import com.dt.sns.model.cms.Article;
import com.dt.sns.service.cms.IArticleService;

import freemarker.template.TemplateException;

/**
 * Created by zchuanzhao on 2017/5/18.
 */
@Component
public class ArticleDirective extends BaseDirective {
	@Resource
	private IArticleService articleService;

	@Override
	public void execute(DirectiveHandler handler) throws TemplateException, IOException {
		int cid = handler.getInteger("cid", 0);
		int num = handler.getInteger("num", 0);
		String sort = handler.getString("sort", "id");
		int day = handler.getInteger("day", 0);
		int thumbnail = handler.getInteger("thumbnail", 0);
		List<Article> list = articleService.listByCustom(cid, sort, num, day, thumbnail);
		handler.put("articleList", list).render();
	}

}
