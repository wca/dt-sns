package com.dt.sns.web.front;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dt.sns.core.util.Const;
import com.dt.sns.core.util.ErrorUtil;
import com.dt.sns.model.cms.Article;
import com.dt.sns.model.cms.ArticleCate;
import com.dt.sns.model.member.Member;
import com.dt.sns.service.cms.IArticleCateService;
import com.dt.sns.service.cms.IArticleCommentService;
import com.dt.sns.service.cms.IArticleService;
import com.dt.sns.service.common.IArchiveService;
import com.dt.sns.util.JeesnsConfig;
import com.dt.sns.util.MemberUtil;
import com.dt.sns.web.BaseController;

/**
 * 个人博客页
 * @author Administrator
 *
 */
@Controller("blogController")
@RequestMapping("/blog")
public class BlogController extends BaseController {
	@Resource
	private JeesnsConfig jeesnsConfig;
	@Resource
	private IArticleCateService articleCateService;
	@Resource
	private IArticleService articleService;
	@Resource
	private IArchiveService archiveService;
	@Resource
	private IArticleCommentService articleCommentService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String list(Model model) {
		int cid = 0;
		int num = 16;
		String sort = "id";
		int day = 0;
		int thumbnail = 0;
		List<Article> list = articleService.listByCustom(cid, sort, num, day, thumbnail);
		model.addAttribute("articleList", list);
		return jeesnsConfig.getFrontTemplate() + "/blog/list";
	}

	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detail(@PathVariable("id") Integer id, Model model) {
		Member loginMember = MemberUtil.getLoginMember(request);
		Article article = articleService.findById(id, loginMember);
		// 文章不存在或者访问未审核的文章，跳到错误页面，提示文章不存在
		if (article == null || article.getStatus() == 0) {
			return jeesnsConfig.getFrontTemplate() + ErrorUtil.error(model, -1009, Const.INDEX_ERROR_FTL_PATH);
		}
		// 更新文章访问次数
		archiveService.updateViewCount(article.getArchiveId());
		model.addAttribute("article", article);
		List<ArticleCate> articleCateList = articleCateService.list();
		model.addAttribute("articleCateList", articleCateList);
		model.addAttribute("loginUser", loginMember);
		return jeesnsConfig.getFrontTemplate() + "/blog/detail";
	}

}
