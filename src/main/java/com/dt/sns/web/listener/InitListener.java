package com.dt.sns.web.listener;

import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.beans.factory.annotation.Autowired;

import com.dt.sns.core.util.Const;
import com.dt.sns.core.util.SpringContextHolder;
import com.dt.sns.model.system.Config;
import com.dt.sns.service.system.IConfigService;
import com.dt.sns.util.JeesnsConfig;

/**
 * Created by zchuanzhao on 2017/5/25.
 */
@WebListener
public class InitListener implements ServletContextListener {

	@Autowired
	IConfigService configService;
	@Autowired
	JeesnsConfig jeesnsConfig;

	public InitListener() {
	}

	public void contextInitialized(ServletContextEvent sce) {
		try {
			Const.PROJECT_PATH = sce.getServletContext().getContextPath();
			sce.getServletContext().setAttribute("basePath", Const.PROJECT_PATH);
			sce.getServletContext().setAttribute("jeesnsConfig", jeesnsConfig);
			String frontTemplate = jeesnsConfig.getFrontTemplate();
			sce.getServletContext().setAttribute("frontTemplate", frontTemplate);
			String managePath = Const.PROJECT_PATH + "/" + jeesnsConfig.getManagePath();
			sce.getServletContext().setAttribute("managePath", managePath);
			List<Config> configList = configService.allList();
			for (Config config : configList) {
				sce.getServletContext().setAttribute(config.getJkey().toUpperCase(), config.getJvalue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void contextDestroyed(ServletContextEvent sce) {

	}
}
