package com.dt.sns.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.dt.sns.web.directive.AdsDirective;
import com.dt.sns.web.directive.ArticleDirective;
import com.dt.sns.web.directive.GroupDirective;
import com.dt.sns.web.directive.GroupTopicDirective;
import com.dt.sns.web.directive.WeiboDirective;

import freemarker.template.TemplateException;
import freemarker.template.utility.XmlEscape;

/*spring.freemarker.allow-request-override=false
spring.freemarker.cache=true
spring.freemarker.check-template-location=true
spring.freemarker.charset=UTF-8
spring.freemarker.content-type=text/html
spring.freemarker.expose-request-attributes=false
spring.freemarker.expose-session-attributes=false
spring.freemarker.expose-spring-macro-helpers=false*/
@Configuration
public class FreeMarkerConfig {

	@Autowired
	protected freemarker.template.Configuration configuration;
//	@Autowired
//	protected org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver resolver;
//	@Autowired
//	protected org.springframework.web.servlet.view.InternalResourceViewResolver springResolver;
	@Autowired
	FreeMarkerConfigurer freemarkerConfig;
	@Autowired
	ArticleDirective articleDirective;
	@Autowired
	WeiboDirective weiboDirective;
	@Autowired
	GroupDirective groupDirective;
	@Autowired
	GroupTopicDirective groupTopicDirective;
	@Autowired
	AdsDirective adsDirective;
	
	@PostConstruct
	public void setSharedVariable() {
		configuration.setDateFormat("yyyy/MM/dd");
		configuration.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");

		// 下面三句配置的就是我自己的freemarker的自定义标签，在这里把标签注入到共享变量中去就可以在模板中直接调用了
		configuration.setSharedVariable("xml_escape", new XmlEscape());
		configuration.setSharedVariable("cms_article_list", articleDirective);
		configuration.setSharedVariable("wb_weibo_list", weiboDirective);

		configuration.setSharedVariable("group_list", groupDirective);
		configuration.setSharedVariable("group_topic_list", groupTopicDirective);
		configuration.setSharedVariable("ads", adsDirective);

		/**
		 * setting配置
		 */
		try {
			configuration.setSetting("template_update_delay", "1");
			configuration.setSetting("default_encoding", "UTF-8");
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}
}
