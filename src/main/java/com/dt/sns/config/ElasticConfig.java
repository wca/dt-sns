package com.dt.sns.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dt.sns.elastic.RestHighLevelClientFactory;

@Configuration
public class ElasticConfig {

	@Bean
	@ConfigurationProperties(prefix = "spring.data.elasticsearch")
	public RestHighLevelClientFactory restHighLevelClient() {
		RestHighLevelClientFactory transportClientFactory = new RestHighLevelClientFactory();
		return transportClientFactory;
	}
}