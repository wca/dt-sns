package com.dt.sns.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dt.sns.core.util.SpringContextHolder;
import com.dt.sns.model.member.Member;
import com.dt.sns.util.JeesnsConfig;
import com.dt.sns.util.MemberUtil;

/**
 * Created by zchuanzhao on 16/11/25.
 */
public class AdminLoginInterceptor implements DTSNSInterceptor {

    @Override
    public boolean interceptor(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws Exception {
        Member loginAdmin = MemberUtil.getLoginMember(httpServletRequest);
        if (loginAdmin == null || loginAdmin.getIsAdmin() == 0) {
            JeesnsConfig jeesnsConfig = SpringContextHolder.getBean("jeesnsConfig");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/" + jeesnsConfig.getManagePath() + "/login");
            return false;
        }
        httpServletRequest.setAttribute("loginUser", loginAdmin);
        return true;
    }
}
