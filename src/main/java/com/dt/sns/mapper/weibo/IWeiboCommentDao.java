package com.dt.sns.mapper.weibo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.weibo.WeiboComment;

/**
 * 微博评论DAO接口 Created by zchuanzhao on 16/12/22.
 */
public interface IWeiboCommentDao extends IBaseDao<WeiboComment> {

	List<WeiboComment> listByWeibo(@Param("page") Page page, @Param("weiboId") Integer weiboId);

	/**
	 * 根据微博ID删除评论
	 * 
	 * @param weiboId
	 * @return
	 */
	int deleteByWeibo(@Param("weiboId") Integer weiboId);
}