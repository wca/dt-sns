package com.dt.sns.mapper.weibo;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.weibo.WeiboFavor;

/**
 * 微博点赞DAO接口 Created by zchuanzhao on 2017/2/8.
 */
public interface IWeiboFavorDao extends IBaseDao<WeiboFavor> {

	WeiboFavor find(@Param("weiboId") Integer weiboId, @Param("memberId") Integer memberId);

	Integer save(@Param("weiboId") Integer weiboId, @Param("memberId") Integer memberId);

	Integer delete(@Param("weiboId") Integer weiboId, @Param("memberId") Integer memberId);
}