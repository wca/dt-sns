package com.dt.sns.mapper.group;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.group.Group;

/**
 * Created by zchuanzhao on 16/12/23.
 */
public interface IGroupDao extends IBaseDao<Group> {

    /**
     * 获取群组
     * @return
     */
    List<Group> listByPage(@Param("page") Page page, @Param("status") Integer status, @Param("key") String key);

    /**
     * 修改状态
     *
     * @param id
     * @return
     */
    Integer changeStatus(@Param("id") Integer id);

    List<Group> listByCustom(@Param("status") int status, @Param("num") int num, @Param("sort") String sort);

}