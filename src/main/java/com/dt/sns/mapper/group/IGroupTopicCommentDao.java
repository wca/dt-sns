package com.dt.sns.mapper.group;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.group.GroupTopicComment;

/**
 * Created by zchuanzhao on 16/12/27.
 */
public interface IGroupTopicCommentDao extends IBaseDao<GroupTopicComment> {

	List<GroupTopicComment> listByGroupTopic(@Param("page") Page page, @Param("groupTopicId") Integer groupTopicId);

	int deleteByTopic(@Param("groupTopicId") Integer groupTopicId);
}