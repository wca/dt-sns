package com.dt.sns.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.model.common.Link;
import com.dt.sns.model.common.Page;

/**
 * Created by zchuanzhao on 2017-10-13.
 */
public interface ILinkDao extends IBaseDao<Link> {

	/**
	 * 分页查询
	 * 
	 * @param page
	 * @return
	 */
	List<Link> listByPage(@Param("page") Page page);

	List<Link> recommentList();

	int enable(@Param("id") Integer id);
}
