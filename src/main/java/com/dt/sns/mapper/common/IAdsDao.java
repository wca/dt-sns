package com.dt.sns.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.model.common.Ads;
import com.dt.sns.model.common.Page;

/**
 * Created by MMF on 2017-09-07.
 */
public interface IAdsDao extends IBaseDao<Ads>{

    /**
     * 分页查询广告信息
     * @param page
     * @return
     */
    List<Ads> listByPage(@Param("page") Page page);

    int enable(@Param("id") Integer id);
}
