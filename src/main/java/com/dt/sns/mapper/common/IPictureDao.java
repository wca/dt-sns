package com.dt.sns.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.model.common.Picture;

/**
 * Created by zchuanzhao on 2017/3/1.
 */
public interface IPictureDao extends IBaseDao<Picture> {

	List<Picture> find(@Param("id") Integer foreignId);

	int delete(@Param("id") Integer foreignId);

	int update(@Param("foreignId") Integer foreignId, @Param("ids") String[] ids);
}