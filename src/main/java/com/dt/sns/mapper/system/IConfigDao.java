package com.dt.sns.mapper.system;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.system.Config;

/**
 * 系统配置信息DAO接口
 * Created by zchuanzhao on 2016/11/26.
 */

public interface IConfigDao extends IBaseDao<Config> {

    boolean update(@Param("key") String key,@Param("value") String value);

    String getValue(@Param("key") String key);
}