package com.dt.sns.mapper.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.system.ActionLog;

/**
 * Created by zchuanzhao on 2017/2/14.
 */
public interface IActionLogDao extends IBaseDao<ActionLog> {

	List<ActionLog> listByPage(@Param("page") Page page, @Param("memberId") Integer memberId);

	List<ActionLog> memberActionLog(@Param("page") Page page, @Param("memberId") Integer memberId);
}
