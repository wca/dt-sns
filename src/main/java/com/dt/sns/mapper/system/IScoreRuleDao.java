package com.dt.sns.mapper.system;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.system.ScoreRule;

/**
 * Created by zchuanzhao on 2017/3/24.
 */
public interface IScoreRuleDao extends IBaseDao<ScoreRule> {

	int enabled(@Param("id") int id);
}
