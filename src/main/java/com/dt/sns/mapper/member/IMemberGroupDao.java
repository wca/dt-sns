package com.dt.sns.mapper.member;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.member.MemGroup;

/**
 * 会员分组DAO Created by zchuanzhao on 16/9/26.
 */
public interface IMemberGroupDao extends IBaseDao<MemGroup> {

}