package com.dt.sns.mapper.member;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.common.Page;
import com.dt.sns.model.member.MemberFans;

/**
 * Created by zchuanzhao on 2017/2/16.
 */
public interface IMemberFansDao extends IBaseDao<MemberFans> {

	List<MemberFans> followsList(@Param("page") Page page, @Param("whoFollowId") Integer whoFollowId);

	List<MemberFans> fansList(@Param("page") Page page, @Param("followWhoId") Integer followWhoId);

	MemberFans find(@Param("whoFollowId") Integer whoFollowId, @Param("followWhoId") Integer followWhoId);

	Integer save(@Param("whoFollowId") Integer whoFollowId, @Param("followWhoId") Integer followWhoId);

	Integer delete(@Param("whoFollowId") Integer whoFollowId, @Param("followWhoId") Integer followWhoId);
}