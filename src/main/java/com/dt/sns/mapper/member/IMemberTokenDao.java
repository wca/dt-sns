package com.dt.sns.mapper.member;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.dt.sns.mapper.common.IBaseDao;
import com.dt.sns.model.member.MemberToken;

/**
 * Created by zchuanzhao on 2017/7/15.
 */
public interface IMemberTokenDao extends IBaseDao<MemberToken> {

	MemberToken getByToken(@Param("token") String token);

	Integer save(@Param("memberId") Integer memberId, @Param("token") String token,
			@Param("expireTime") Date expireTime);

}